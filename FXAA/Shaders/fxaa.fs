#version 120

// Big thanks to benny: http://www.youtube.com/watch?v=Z9bYzpwVINA&list=UUnlpv-hhcsAtEHKR2y2fW4Q
// Also, http://blog.codinghorror.com/fast-approximate-anti-aliasing-fxaa/

varying vec2 texCoord;
uniform sampler2D texture;
uniform vec2 invTextureSize;

void main() {
    float spanMax = 8.0f; // Clamp magnitude
    const float reduceMin = 1.0 / 128.0; // Avoid div by 0
    float reduceMul = 1.0 / spanMax; // Bias direction
    
    /* Get luminosity */
    vec3 luminosity = vec3(0.233, 0.587, 0.114); // There be dragons!
    
    // Get fragment luminosity
    float topL      = dot(texture2D(texture, texCoord + vec2(-1.0,-1.0) * invTextureSize).xyz, luminosity);
    float topR      = dot(texture2D(texture, texCoord + vec2( 1.0,-1.0) * invTextureSize).xyz, luminosity);
    float bottomL   = dot(texture2D(texture, texCoord + vec2(-1.0, 1.0) * invTextureSize).xyz, luminosity);
    float bottomR   = dot(texture2D(texture, texCoord + vec2( 1.0, 1.0) * invTextureSize).xyz, luminosity);
    float middle    = dot(texture2D(texture, texCoord).xyz, luminosity); // Draw for luminosity
    
    // Find luminosity range
    float luminosityMin = min(middle, min(min(topL, topR), min(bottomL, bottomR)));
    float luminosityMax = max(middle, max(max(topL, topR), max(bottomL, bottomR)));
    
    /* Figure out blur direction and magnitude */
    // Reduce to avoid division by 0
    // Offset bias (1 / 8) by multiplying it with average luminosity
    float dirReduce = max((topL + topR + bottomL + bottomR) * (0.25 * reduceMul), reduceMin);
    
    // Get blur direction (and magnitude)
    vec2 dir = vec2(-((topL + topR) - (bottomL + bottomR)), (topL + bottomL) - (topR + bottomR)); // Draw to see what gets blured
    
    // Determine magnitude (Scale so smallest component is length of 1)
    float invDirAdjustment = 1.0f / (min(abs(dir.x), abs(dir.y)) + dirReduce); // +dirReduce to avoid division by 0
    
    // Clamp the max magnitude to 8
    dir = min(vec2(spanMax, spanMax), max(vec2(-spanMax, -spanMax), dir * invDirAdjustment));
    
    /* Perform actual blur */
    dir = dir * invTextureSize; // Go into texel space
    
    vec3 result1 = 0.5 * (
       texture2D(texture, texCoord + dir * vec2(1.0/3.0 - 0.5)).xyz +
       texture2D(texture, texCoord + dir * vec2(2.0/3.0 - 0.5)).xyz
    ); // 1 / 6 on both sides
    
    vec3 result2 = 0.25 * (
        texture2D(texture, texCoord + dir * vec2(0.0/3.0 - 0.5)).xyz +
        texture2D(texture, texCoord + dir * vec2(3.0/3.0 - 0.5)).xyz
    );
    
    // Combine results
    vec3 finalResult = result1 * 0.5 + result2;
    
    // Test if we've sampled too far (If its outside the range of other luminosities)
    float luminosityResult = dot(luminosity, finalResult);
    
    if (luminosityResult < luminosityMin || luminosityResult > luminosityMax) // Outside range?
        gl_FragColor = vec4(result1, 1.0);
    else
        gl_FragColor = vec4(finalResult, 1.0);
}