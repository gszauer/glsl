#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Obj.h"
#include "Shader.h"
#include "Camera.h"
#include <sstream>

int GetWidth() { return 800; }
int GetHeight() { return 600; }

Obj* crate;
Obj* susane;
Obj* deathstroke;

Shader* solidColorShader;
Shader* diffuseTextureShader;
Shader* fxaaShader;

Texture* deathstrokeDiffuse;
PostProcess* fxaaFilter;
Camera* camera;

glm::vec3 camPosition = glm::vec3(5.0f, 2.0f, 10.0f);
bool g_bRenderFromCamer = false;

bool Initialize() {
    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 500.0f);
    deathstrokeDiffuse = new Texture("Assets/Deathstroke_D.png");
    
    crate = new Obj("Assets/Cube.obj");
    susane = new Obj("Assets/Monkey.obj");
    deathstroke = new Obj("Assets/Deathstroke.obj");
    
    solidColorShader = new Shader();
    solidColorShader->SetVertexShaderFromFile("Shaders/solid.vs");
    solidColorShader->SetFragmentShaderFromFile("Shaders/solid.fs");
    solidColorShader->Prepare();
    
    diffuseTextureShader = new Shader();
    diffuseTextureShader->SetVertexShaderFromFile("Shaders/diffuse.vs");
    diffuseTextureShader->SetFragmentShaderFromFile("Shaders/diffuse.fs");
    diffuseTextureShader->Prepare();
    
    fxaaShader = new Shader();
    fxaaShader->SetVertexShaderFromFile("Shaders/fxaa.vs");
    fxaaShader->SetFragmentShaderFromFile("Shaders/fxaa.fs");
    fxaaShader->Prepare();
    
    fxaaFilter = new PostProcess(GetWidth(), GetHeight(), camera->GetRenderTarget(), fxaaShader);
    
    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
    camera->LookAt(camPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 500.0f);
    camera->UpdateFrameBuffer(w, h);
    fxaaFilter->Resize(w, h);
    fxaaFilter->SetInput(camera->GetRenderTarget());
    fxaaFilter->SetShader(fxaaShader);
    PrintGLError();
}

void Render() {
    camera->Bind();

    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);

    solidColorShader->Bind();

    solidColorShader->Mat4("view", camera->GetView());
    solidColorShader->Mat4("projection", camera->GetProjection());

    solidColorShader->Mat4("model", glm::translate(glm::vec3(-5.0f, 0.0f, -3.0f)));
    solidColorShader->Vec3("color", glm::vec3(1.0f, 1.0f, 0.0f));
    crate->Render(solidColorShader->GetAttribLocation("position"));

    solidColorShader->Mat4("model", glm::translate(glm::vec3(1.0f, 1.0f, 0.0f)));
    solidColorShader->Vec3("color", glm::vec3(1.0f, 0.0f, 0.0f));
    susane->Render(solidColorShader->GetAttribLocation("position"));
    
    solidColorShader->Unbind();
    
    diffuseTextureShader->Bind();
    diffuseTextureShader->Mat4("view", camera->GetView());
    diffuseTextureShader->Mat4("projection", camera->GetProjection());
    diffuseTextureShader->Mat4("model", glm::translate(glm::vec3(5.0f, -1.0f, 6.0f)));
    diffuseTextureShader->Texture("diffuse", *deathstrokeDiffuse);
    deathstroke->Render(diffuseTextureShader->GetAttribLocation("position"), -1, diffuseTextureShader->GetAttribLocation("uv"));
    diffuseTextureShader->Unbind();
    
    camera->Unbind();

    if (g_bRenderFromCamer) {
        camera->BlitToScreen(false);
    }
    else {
        fxaaFilter->Bind();
        fxaaFilter->GetShader()->Vec2("invTextureSize", glm::vec2(1.0f / fxaaFilter->GetResult()->GetWidth(), 1.0f / fxaaFilter->GetResult()->GetWidth()));
        fxaaFilter->Execute();
        fxaaFilter->Unbind();
        fxaaFilter->BlitToScreen();
    }
}

bool Update(float dt) {
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete fxaaFilter;
    delete fxaaShader;
    delete crate;
    delete deathstroke;
    delete camera;
    delete susane;
    delete diffuseTextureShader;
    delete solidColorShader;
    delete deathstrokeDiffuse;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        camera->SaveToPng();
        fxaaFilter->SaveToPng();
    }
    else if (key == 30) {
        g_bRenderFromCamer = true;
    }
    else if (key == 31) {
        g_bRenderFromCamer = false;
    }
    else if (key == 21) {
        delete fxaaShader;
        fxaaShader = new Shader();
        fxaaShader->SetVertexShaderFromFile("Shaders/fxaa.vs");
        fxaaShader->SetFragmentShaderFromFile("Shaders/fxaa.fs");
        fxaaShader->Prepare();
        fxaaFilter->SetShader(fxaaShader);
    }
    else if (key != 41) {
        std::cout << "Key pressed: " << key << "\n";
    }
}
