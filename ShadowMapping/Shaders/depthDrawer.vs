#version 120
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 _position;
attribute vec3 _normal;

void main() {
	vec4 position = vec4(_normal, 1.0); // Just so _normal is not optimized out!
	vec4 pos2 =  projection * view * model * vec4(_position, 1.0f);
    gl_Position = (position + pos2) - position;
}