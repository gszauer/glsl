#version 120

struct Light {
	vec4 position;	// x, y, z, directional radius
	vec4 color;		// r, g, b
	vec4 specular;	// r, g, b, spec power
};

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform sampler2D shadowMap;
uniform Light lights[4];
uniform vec3 ambientColor;
uniform int numLights;

vec3 DirectionalLight(vec3 vertWorldNormal, vec3 lightWorldDir, vec3 lightColor, vec3 cameraDir, float specPower, vec3 specColor) {
	vec3 N = vertWorldNormal;
	vec3 L = lightWorldDir;
	float NdotL = max(dot(N, L), 0.0);
	vec3 diffuseColor = lightColor * NdotL;
	
	vec3 specularColor = vec3(0.0);
	if (specPower > 0.0) {
		vec3 reflection = normalize(vertWorldNormal + lightWorldDir * -0.5);
	    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);
	    specularColor = specColor * spec;
    }

	return ambientColor + diffuseColor + specularColor;
}

vec3 PointLight(vec3 vertWorldNormal, vec3 vertWorldPosition, vec3 lightWorldPos, vec3 lightColor, float radius, vec3 cameraDir, float specPower, vec3 specColor) {
	vec3 N = vertWorldNormal;
	vec3 L = normalize(lightWorldPos - vertWorldPosition);
	float NdotL = max(dot(N, L), 0.0);
	
	float distance = length(lightWorldPos - vertWorldPosition);
	float attenuation = 1.0 - (pow(distance, 2) / pow(radius, 2.0));
	
	vec3 diffuseColor = lightColor * NdotL * attenuation;

	vec3 specularColor = vec3(0.0);
	if (specPower > 0.0) {
		vec3 reflection = normalize(vertWorldNormal + L * -0.5);
	    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);
	    specularColor = specColor * spec * attenuation;
    }

    return ambientColor + diffuseColor + specularColor;
}

// Lighting data input
varying vec3 lightWorldDirection[4];
varying vec3 cameraDirection;
varying vec3 worldSpaceVertex;
varying vec2 texCoord;
varying mat3 tbnMatrix;
varying vec4 shadowCoord;

vec3 ProcessLight(int lightIndex, vec3 norm) {
	vec3 color = vec3(0);
	
	if (lights[lightIndex].position.w == 0) { // No radius, assume a point light!
		color = DirectionalLight(norm, lightWorldDirection[lightIndex], lights[lightIndex].color.xyz, cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz);
	}
	else {
		color = PointLight(norm, worldSpaceVertex, lights[lightIndex].position.xyz, lights[lightIndex].color.xyz, lights[lightIndex].position.w, cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz);
	}
	return color;
}

void main() {
	vec2 sample = texCoord.xy;

	vec3 litColor = vec3(0.0);
	vec3 norm = normalize(tbnMatrix * ((255.0 / 128.0) * texture2D(normalMap, sample).xyz - 1.0));

	float shadow = 1.0;
	bool canShadow = shadowCoord.w > 0.0;
	vec4 shadowCoordinateWdivide = vec4(0.0);
	float distanceFromLight = 0.0;
	float bias = 0.0005f; // We don't use a slope based bias here, assuming that the normal map has sufficient resolution
	if (canShadow) {
		shadowCoordinateWdivide = shadowCoord / shadowCoord.w;
		distanceFromLight = texture2D(shadowMap,shadowCoordinateWdivide.st).z;
		// Because the geom being rendered might not be in the shadow map (Far plane might not cover whole geometry)
		// We must treat anything that is distanceFromLight == 1 (on the far clip plane) as not shadowed.
	}

	if (numLights >= 1) {
		litColor += ProcessLight(0, norm);
		if (canShadow && distanceFromLight < 1.0 && distanceFromLight < shadowCoordinateWdivide.z - bias)
			shadow -= 0.5;
	}
	if (numLights >= 2) {
		litColor += ProcessLight(1, norm);
		if (canShadow && distanceFromLight < 1.0 && distanceFromLight < shadowCoordinateWdivide.z - bias)
			shadow -= 0.5;
	}
	if (numLights >= 3) {
		litColor += ProcessLight(2, norm);
		if (canShadow && distanceFromLight < 1.0 && distanceFromLight < shadowCoordinateWdivide.z - bias)
			shadow -= 0.5;
	}
	if (numLights >= 4) {
		litColor += ProcessLight(3, norm);
		if (canShadow && distanceFromLight < 1.0 && distanceFromLight < shadowCoordinateWdivide.z - bias)
			shadow -= 0.5;
	}

	gl_FragColor = vec4((litColor * vec3(texture2D(diffuseMap, sample))) * max(shadow, 0.0), 1.0);

	//gl_FragColor.r = gl_FragColor.g = gl_FragColor.b = shadow;
}