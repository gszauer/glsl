#version 120

struct Light {
	vec4 position;	// x, y, z, directional radius
	vec4 color;		// r, g, b
	vec4 specular;	// r, g, b, spec power
};

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 shadowBias;
uniform mat4 shadowProjection;
uniform mat4 shadowView;
uniform mat4 shadowModel;
uniform Light lights[4];
uniform int numLights;
uniform vec3 color;

attribute vec3 _position;
attribute vec3 _normal;

// Lighting data output
varying vec3 lightWorldDirection[4];
varying vec3 vertexWorldNormal;
varying vec3 cameraDirection;
varying vec3 worldSpaceVertex;
varying vec3 vertexColor;
varying vec4 shadowCoord;

void main() {
	shadowCoord = shadowBias * shadowProjection * shadowView * shadowModel * vec4(_position, 1.0); 
	mat3 normalMatrix = mat3(model);
	vec4 vertex = projection * view * model * vec4(_position, 1.0);
	vec3 eyePosition = vec3(view[3][0], view[3][1], view[3][2]); 

	// Light Agnostic
	cameraDirection = normalize(eyePosition - vertex.xyz);
    worldSpaceVertex = vec3(model * vec4(_position, 1.0));
    vertexWorldNormal = normalize(normalMatrix * _normal);
    vertexColor = color;

    // Light Specific
    if (numLights >= 1)
		lightWorldDirection[0] = normalize(lights[0].position.xyz); 
	if (numLights >= 2)
		lightWorldDirection[1] = normalize(lights[1].position.xyz); 
	if (numLights >= 3)
		lightWorldDirection[2] = normalize(lights[2].position.xyz); 
	if (numLights >= 4)
		lightWorldDirection[3] = normalize(lights[3].position.xyz); 

    gl_Position = vertex;
}