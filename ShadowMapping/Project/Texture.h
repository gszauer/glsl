#ifndef _H_TEXTURE_
#define _H_TEXTURE_

class Texture { // 24 bit, RGBA
public:
    enum MinFilter { MIN_NEAREST_NEAREST, MIN_LINEAR_NEAREST, MIN_NEAREST_LINEAR, MIN_LINEAR_LINEAR, MIN_NEAREST, MIN_LINEAR };
    enum MagFilter { MAG_LINEAR, MAG_NEAREST };
    enum TextureWrap { CLAMP, MIRRORED, REPEAT };
    enum Format { DEPTH, RGBA_24 };
protected:
    unsigned int    m_nTextureId;
    MinFilter       m_eMinFilter;
    MagFilter       m_eMagFilter;
    TextureWrap     m_eTextureWrap;
    Format          m_eFormat;
    unsigned int    m_nTextureWidth;
    unsigned int    m_nTextureHeight;
protected:
    Texture();
    Texture(const Texture&);
    Texture& operator=(const Texture&);
    unsigned int LoadImage(const char* path, MinFilter min, MagFilter mag, TextureWrap wrap, bool mips);
public:
    Texture(unsigned int width, unsigned int height, Format format);
    Texture(const char* path, bool mipmaps = false);
    Texture(const char* path, TextureWrap wrap, bool mipmaps = false);
    Texture(const char* path, MinFilter min, MagFilter mag, bool mipmaps = false);
    Texture(const char* path, MinFilter min, MagFilter mag, TextureWrap wrap, bool mipmaps = false);
    ~Texture();

    unsigned int GetHandle();
    MinFilter GetMin();
    MagFilter GetMag();
    TextureWrap GetWrapMode();
    Format GetFormat();
    unsigned int GetWidth();
    unsigned int GetHeight();

    void SetMin(MinFilter min);
    void SetMag(MagFilter mag);
    void SetWrap(TextureWrap wrap);
    void ClampToColor(float r, float g, float b);

    void SavePNG(const char* path, float _n = 0.0f, float _f = 1.0f);
};

#endif
