#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Shader.h"
#include "Camera.h"
#include "OBJ.h"
#include <sstream>

// specularColor.xyz = Color info
// specularColor.w = specular power
// diffuseColor.xyz = Color info
// diffuseColor.w = unused!
// globalPosition.xyz = SpatialPosition
// globalPosition.w = radius

#define TO_RAD              0.0174532925f
#define TRANSLATE_SPEED     5.0f
#define PRINT_VEC(vec)        ": " << vec.x << ", " << vec.y << ", " << vec.z

#define VIEW_CAMERA         0
#define ORTHO_SIZE          7
#define SHADOWMAP_SIZE      2048

int GetWidth() { return 800; }
int GetHeight() { return 600; }

glm::vec3 cameraPosition;
glm::vec2 cameraAngle;

glm::vec3 forward;
glm::vec3 right;
glm::vec3 up;

Camera* camera;
int g_nLightStatus = 2;

Camera* shadowCamera;
Shader* depthDrawShader;

float objRotation = 0.0f;

struct PointLight {
    glm::vec4 globalPosition;
    glm::vec4 diffuseColor;
    glm::vec4 specularColor;

    Shader* rs;
    OBJ* vs;

    PointLight() {
        rs = new Shader();
        rs->SetVertexShader("#version 120 \n uniform mat4 model; uniform mat4 view; uniform mat4 projection; attribute vec3 position; void main() { gl_Position = projection * view * model * vec4(position, 1.0f); } ");
        rs->SetFragmentShader("#version 120 \n uniform vec4 color; void main() { gl_FragColor = vec4(color.xyz, 0.5); }");
        rs->Prepare();

        vs = new OBJ("Assets/Sphere.obj");
    }

    ~PointLight() {
        delete rs;
        delete vs;
    }

    void Render() { // Pos * Rot * Scale
        if (g_nLightStatus == 2) return;

        if (g_nLightStatus == 1) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glBlendEquation(GL_FUNC_ADD);
        }

        rs->Bind();
        rs->Mat4("view", camera->GetView());
        rs->Mat4("projection", camera->GetProjection());
        glm::vec3 gPosition = glm::vec3(globalPosition.x, globalPosition.y, globalPosition.z);
        rs->Mat4("model", glm::translate(gPosition) * glm::scale(glm::vec3(globalPosition.w)));
        rs->Vec4("color", diffuseColor);
        vs->Render(rs->GetAttribLocation("position"));
        rs->Unbind();

        if (g_nLightStatus == 1)
            glDisable(GL_BLEND);
    }
};

glm::vec3 ambientLight = glm::vec3(0.1f);
glm::vec3 camPosition = glm::vec3(0.0f, 0.0f, -10.0f);
PointLight* light1;
PointLight* light2;
PointLight* light3;

OBJ* model;
OBJ* plane;
Shader* normalMappedShader;
Shader* solidColorShader;
Texture* bricks;
Texture* bricks_n;

bool Initialize() {
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);

    forward = glm::vec3(0.0f, 0.0f, 1.0f);
    up = glm::vec3(0.0f, 1.0f, 0.0f);
    right = glm::vec3(1.0f, 0.0f, 0.0f);

    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 200.0f);
    cameraPosition = glm::vec3(0.0f, 3.0f, -10.0f);
    cameraAngle = glm::vec2(0.0f, 0.0f);

    shadowCamera = new Camera(SHADOWMAP_SIZE, SHADOWMAP_SIZE, Camera::DEPTH, -ORTHO_SIZE, ORTHO_SIZE, ORTHO_SIZE, -ORTHO_SIZE, -ORTHO_SIZE, ORTHO_SIZE);

    light1 = new PointLight();
    light2 = new PointLight();
    light3 = new PointLight();

    light1->diffuseColor = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f); // r, g, b, unused
    light1->specularColor = glm::vec4(0.0f, 1.0f, 1.0f, 36.0f); // r, g, b, spec power
    light1->globalPosition = glm::vec4(1.5f, 1.0f, 0.0f, 0.0f); // x, y, z, radius

    model = new OBJ("Assets/susane.obj");
    plane = new OBJ("Assets/plane.obj");

    bricks = new Texture("Assets/bricks2.jpg", Texture::REPEAT);
    bricks_n = new Texture("Assets/bricks2_normal.jpg", Texture::REPEAT);

    normalMappedShader = new Shader();
    normalMappedShader->SetVertexShaderFromFile("Shaders/normalMapped.vs");
    normalMappedShader->SetFragmentShaderFromFile("Shaders/normalMapped.fs");
    normalMappedShader->Prepare();

    solidColorShader = new Shader();
    solidColorShader->SetVertexShaderFromFile("Shaders/solidColor.vs");
    solidColorShader->SetFragmentShaderFromFile("Shaders/solidColor.fs");
    solidColorShader->Prepare();

    depthDrawShader = new Shader();
    depthDrawShader->SetVertexShaderFromFile("Shaders/depthDrawer.vs");
    depthDrawShader->SetFragmentShaderFromFile("Shaders/depthDrawer.fs");
    depthDrawShader->Prepare();

    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
#if VIEW_CAMERA
    camera->Ortho(-ORTHO_SIZE, ORTHO_SIZE, ORTHO_SIZE, -ORTHO_SIZE, -ORTHO_SIZE, ORTHO_SIZE);
#else
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 200.0f);
#endif
    camera->UpdateFrameBuffer(w, h);

    shadowCamera->Ortho(-ORTHO_SIZE, ORTHO_SIZE, ORTHO_SIZE, -ORTHO_SIZE, -ORTHO_SIZE, ORTHO_SIZE);
    shadowCamera->UpdateFrameBuffer(SHADOWMAP_SIZE, SHADOWMAP_SIZE);

    PrintGLError();
}

void ImLine(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& c) {
    glBegin(GL_LINES);
    glColor3f(c.x, c.y, c.z);
    glVertex3f(p1.x, p1.y, p1.z);
    glColor3f(c.x, c.y, c.z);
    glVertex3f(p2.x, p2.y, p2.z);
    glEnd();
}

void Render() {
    glm::quat rotation = glm::quat(glm::vec3(cameraAngle.y * TORAD, cameraAngle.x * TORAD, 0.0f));
    glm::vec3 shadowCameraPosition = glm::normalize(glm::vec3(light1->globalPosition.x, light1->globalPosition.y, light1->globalPosition.z)) * 5.0f;
#if VIEW_CAMERA
    camera->LookAt(shadowCameraPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
#else
    camera->LookAt(cameraPosition, cameraPosition + glm::normalize(forward * rotation), glm::normalize(up * rotation));
#endif
    shadowCamera->LookAt(shadowCameraPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

    glm::mat4 monkeyMat = glm::mat4_cast(glm::quat(glm::vec3(0.0f, objRotation * TORAD, 0.0f)));
    glm::mat4 planeMat = glm::translate(glm::vec3(4.0, -1.0, 0.0));

    shadowCamera->Bind(true);
    glViewport(0, 0, shadowCamera->GetDepthTarget()->GetWidth(), shadowCamera->GetDepthTarget()->GetHeight());
    glClearDepth(1.0f);
    glCullFace(GL_FRONT);
    glClear(GL_DEPTH_BUFFER_BIT);
    depthDrawShader->Bind();
    depthDrawShader->Mat4("view", shadowCamera->GetView());
    depthDrawShader->Mat4("projection", shadowCamera->GetProjection());
    depthDrawShader->Mat4("model", monkeyMat);
    model->Render(depthDrawShader->GetAttribLocation("_position"), depthDrawShader->GetAttribLocation("_normal"));
    depthDrawShader->Unbind();
    PrintGLError();
    shadowCamera->Unbind();
    glCullFace(GL_BACK);
    PrintGLError();

    camera->Bind();
    glViewport(0, 0, GetWidth(), GetHeight());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render normal mapped ground
    normalMappedShader->Bind();
    normalMappedShader->Mat4("view", camera->GetView());
    normalMappedShader->Mat4("projection", camera->GetProjection());
    normalMappedShader->Mat4("model", planeMat);

    normalMappedShader->Mat4("shadowBias", glm::mat4(   0.5, 0.0, 0.0, 0.0,
                                                        0.0, 0.5, 0.0, 0.0,
                                                        0.0, 0.0, 0.5, 0.0,
                                                        0.5, 0.5, 0.5, 1.0));
    normalMappedShader->Mat4("shadowProjection", shadowCamera->GetProjection());
    normalMappedShader->Mat4("shadowView", shadowCamera->GetView());
    normalMappedShader->Mat4("shadowModel", planeMat);

    normalMappedShader->Int("numLights", 1);
    normalMappedShader->Vec3("ambientColor", ambientLight);
    normalMappedShader->Vec4 ("lights[0].position", light1->globalPosition);
    normalMappedShader->Vec4 ("lights[0].color", light1->diffuseColor);
    normalMappedShader->Vec4 ("lights[0].specular", light1->specularColor);
    normalMappedShader->Texture("diffuseMap", *bricks);
    normalMappedShader->Texture("normalMap", *bricks_n);
    normalMappedShader->Texture("shadowMap", *shadowCamera->GetDepthTarget());
#if !VIEW_CAMERA
    plane->Render(normalMappedShader->GetAttribLocation("_position"), normalMappedShader->GetAttribLocation("_normal"), normalMappedShader->GetAttribLocation("_uv"), normalMappedShader->GetAttribLocation("_tangent"));
#endif
    normalMappedShader->Unbind();
    // Render solid monkey
    solidColorShader->Bind();
    solidColorShader->Mat4("view", camera->GetView());
    solidColorShader->Mat4("projection", camera->GetProjection());
    solidColorShader->Mat4("model", monkeyMat);
    solidColorShader->Int("numLights", 1);
    solidColorShader->Vec3("ambientColor", ambientLight);
    solidColorShader->Vec4 ("lights[0].position", light1->globalPosition);
    solidColorShader->Vec4 ("lights[0].color", light1->diffuseColor);
    solidColorShader->Vec4 ("lights[0].specular", light1->specularColor);
    solidColorShader->Vec3("color", glm::vec3(1.0f, 0.0f, 0.0f));
    solidColorShader->Mat4("shadowBias", glm::mat4( 0.5, 0.0, 0.0, 0.0,
                                                    0.0, 0.5, 0.0, 0.0,
                                                    0.0, 0.0, 0.5, 0.0,
                                                    0.5, 0.5, 0.5, 1.0));
    solidColorShader->Mat4("shadowProjection", shadowCamera->GetProjection());
    solidColorShader->Mat4("shadowView", shadowCamera->GetView());
    solidColorShader->Mat4("shadowModel", monkeyMat);
    solidColorShader->Texture("shadowMap", *shadowCamera->GetDepthTarget());
    model->Render(solidColorShader->GetAttribLocation("_position"), solidColorShader->GetAttribLocation("_normal"));
    solidColorShader->Unbind();
    // Done Rendering
    PrintGLError();
    light1->Render();
    PrintGLError();
    camera->Unbind();

    //shadowCamera->BlitToScreen(true, true);
    camera->BlitToScreen(false);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(glm::value_ptr(camera->GetProjection()));
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(glm::value_ptr(camera->GetView()));
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    ImLine(glm::vec3(0), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    ImLine(glm::vec3(0), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    ImLine(glm::vec3(0), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    glm::vec3 gPos = glm::vec3(light1->globalPosition.x, light1->globalPosition.y, light1->globalPosition.z);
    ImLine(glm::vec3(0), gPos, glm::vec3(1.0f, 1.0f, 0.0f));
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

bool Update(float dt) {
    objRotation += dt * 36.0f;
    while (objRotation > 360.0f)
        objRotation -= 360.0f;
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete light1;
    delete light2;
    delete light3;
    delete camera;
    delete shadowCamera;
    delete depthDrawShader;
    delete model;
    delete plane;
    delete bricks;
    delete bricks_n;
    delete normalMappedShader;
    delete solidColorShader;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        camera->SaveToPng();
        //shadowCamera->SaveToPng();
    }
    else if (key == 30) {
        if (++g_nLightStatus > 2)
            g_nLightStatus = 0;
    }
    else if (key == 31) {
        camPosition = glm::vec3(0.0f, 0.0f, -10.0f);
    }
    else if (key != 41 && key != 4 && key != 7 && key != 22 && key != 26 &&
             key != 12 && key != 13 && key != 14 && key != 15) {
        std::cout << "Key pressed: " << key << "\n";
    }

    { // FREE CAMERA
        if (key == 14) { // K
            cameraAngle.y -= 36.0f * dt;
            if (cameraAngle.y < -60.0f)
                cameraAngle.y = -60.0f;
        }
        else if (key == 13) { // J
            cameraAngle.x -= 36.0f * dt;
            while (cameraAngle.x < 0.0f)
                cameraAngle += 360.0f;
        }
        else if (key == 12) { // I
            cameraAngle.y += 36.0f * dt;
            if (cameraAngle.y > 60.0f)
                cameraAngle.y = 60.0f;
        }
        else if (key == 15) { // L
            cameraAngle.x += 36.0f * dt;
            while (cameraAngle.x > 360.0f)
                cameraAngle -= 360.0f;
        }

        glm::quat rotation = glm::quat(glm::vec3(cameraAngle.y * TORAD, cameraAngle.x * TORAD, 0.0f));

        if (key == 4) { // A
            cameraPosition += (right * rotation) * TRANSLATE_SPEED * dt;
        }
        else if (key == 7) { // D
            cameraPosition -= (right * rotation) * TRANSLATE_SPEED * dt;
        }
        if (key == 22) { // S
            cameraPosition -= (forward * rotation) * TRANSLATE_SPEED * dt;
        }
        else if (key == 26) { // W
            cameraPosition += (forward * rotation) * TRANSLATE_SPEED * dt;
        }
    }

}
