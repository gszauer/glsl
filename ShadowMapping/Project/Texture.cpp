#include "Texture.h"
#include "stb_image.h"
#include "OGLHeaders.h"
#include <vector>
#include "lodepng.h"
#include <iostream>

// http://www.opengl.org/discussion_boards/showthread.php/167244-Reading-back-pixels-(texture)-using-FBO

Texture::Texture(unsigned int width, unsigned int height, Format format) {
    if (format == DEPTH) {
        m_eMinFilter = MIN_NEAREST;
        m_eMagFilter = MAG_NEAREST;
        m_eTextureWrap = CLAMP;
        m_eFormat = format;
        m_nTextureWidth = width;
        m_nTextureHeight = height;

        // Create depth texture component
        glGenTextures(1, &m_nTextureId);
        glBindTexture(GL_TEXTURE_2D, m_nTextureId);

        // GL_LINEAR does not make sense for a depth texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // Remove artifacts on edges of the shadow map
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);

        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);

        // No need to force GL_DEPTH_COMPONENT24, drivers usually give you the max precision if available
        glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    else {
        m_eMinFilter = MIN_NEAREST;
        m_eMagFilter = MAG_NEAREST;
        m_eTextureWrap = CLAMP;
        m_eFormat = format;
        m_nTextureWidth = width;
        m_nTextureHeight = height;

        // Create depth texture component
        glGenTextures(1, &m_nTextureId);
        glBindTexture(GL_TEXTURE_2D, m_nTextureId);


        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
        glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

Texture::Texture(const char* path, bool mipmaps) {
    LoadImage(path, MIN_LINEAR, MAG_LINEAR, CLAMP, mipmaps);
}

Texture::Texture(const char* path, TextureWrap wrap, bool mipmaps) {
    LoadImage(path, MIN_LINEAR, MAG_LINEAR, wrap, mipmaps);
}

Texture::Texture(const char* path, MinFilter min, MagFilter mag, bool mipmaps) {
    LoadImage(path, min, mag, CLAMP, mipmaps);
}

Texture::Texture(const char* path, MinFilter min, MagFilter mag, TextureWrap wrap, bool mipmaps) {
    LoadImage(path, min, mag, wrap, mipmaps);
}

Texture::~Texture() {
    glDeleteTextures(1, &m_nTextureId);
}

unsigned int Texture::LoadImage(const char* path, MinFilter min, MagFilter mag, TextureWrap wrap, bool mips) {
    int bitdepth = 0;
    int width, height;

    unsigned char *data_raw = stbi_load(path, &width, &height, &bitdepth, 4);
    if (data_raw == 0) {
        std::cout << "Couldn't load texture: " << path << "\n";
        return 0;
    }

    int u2 = 1;
    int v2 = 1;
    while(u2 < width) u2 *= 2;
    while(v2 < height) v2 *= 2;

    std::vector<unsigned char> image(u2 * v2 * 4);
    for(size_t y = 0; y < height; y++) {
        for(size_t x = 0; x < width; x++) {
            for(size_t c = 0; c < 4; c++) {
                image[4 * u2 * y + 4 * x + c] = data_raw[4 * width * y + 4 * x + c];
            }
        }
    }

    width = u2;
    height = v2;
    const unsigned char* data = &image[0];

    GLuint handle;
    glGenTextures(1, &handle);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, handle);

    if (min == MIN_NEAREST_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    else if (min == MIN_LINEAR_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    else if (min == MIN_NEAREST_LINEAR)
        glTexParameterf(MIN_LINEAR_LINEAR, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    else if (min == MIN_LINEAR_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else if (min == MIN_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    else if (min == MIN_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    if (mag == MAG_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    else if (mag == MAG_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (wrap == CLAMP) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    else if (wrap == MIRRORED) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    }
    else if (wrap == REPEAT) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    if (mips)
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)data);

    stbi_image_free(data_raw);

    m_nTextureId = handle;
    m_eMinFilter = min;
    m_eMagFilter = mag;
    m_eTextureWrap = wrap;
    m_eFormat = RGBA_24;
    m_nTextureWidth = width;
    m_nTextureHeight = height;

    glBindTexture(GL_TEXTURE_2D, handle);

    return handle;
}

void Texture::SetMin(MinFilter min) {
    glBindTexture(GL_TEXTURE_2D, m_nTextureId);
    if (min == MIN_NEAREST_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    else if (min == MIN_LINEAR_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    else if (min == MIN_NEAREST_LINEAR)
        glTexParameterf(MIN_LINEAR_LINEAR, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    else if (min == MIN_LINEAR_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else if (min == MIN_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    else if (min == MIN_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_eMinFilter = min;
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SetMag(MagFilter mag) {
    glBindTexture(GL_TEXTURE_2D, m_nTextureId);
    if (mag == MAG_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    else if (mag == MAG_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_eMagFilter = mag;
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::ClampToColor(float r, float g, float b) {
    glBindTexture(GL_TEXTURE_2D, m_nTextureId);

    float color[] = {r, g, b, 0};

    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SetWrap(TextureWrap wrap) {
    glBindTexture(GL_TEXTURE_2D, m_nTextureId);
    if (wrap == CLAMP) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    else if (wrap == MIRRORED) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    }
    else if (wrap == REPEAT) {
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    m_eTextureWrap = wrap;
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::SavePNG(const char* path, float _near, float _far) {
    std::vector<unsigned char> data;
    data.resize(m_nTextureWidth * m_nTextureHeight * 4);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_nTextureId);
    if (m_eFormat == RGBA_24) {
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, &data[0]);
    }
    else {
        std::vector<float> depthData;
        depthData.resize(m_nTextureWidth * m_nTextureHeight);
        data.resize(0); data.reserve(m_nTextureWidth * m_nTextureHeight * 4);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, &depthData[0]);

        for (int i = 0, isize = int(depthData.size()); i < isize; ++i) {
            float depth = depthData[i];
            depth = (2.0f * _near) / (_far + _near - depth * (_far - _near));

            data.push_back((unsigned char)(depth * float(UCHAR_MAX)));
            data.push_back((unsigned char)(depth * float(UCHAR_MAX)));
            data.push_back((unsigned char)(depth * float(UCHAR_MAX)));
            data.push_back(UCHAR_MAX);
        }
    }

    glDisable(GL_TEXTURE_2D);

    unsigned error = lodepng::encode(path, data, m_nTextureWidth, m_nTextureHeight);
    if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
    else std::cout << "Saved: " << path << "\n";
}

unsigned int Texture::GetHandle() {
    return m_nTextureId;
}

Texture::MinFilter Texture::GetMin() {
    return m_eMinFilter;
}

Texture::MagFilter Texture::GetMag() {
    return m_eMagFilter;
}

Texture::TextureWrap Texture::GetWrapMode() {
    return m_eTextureWrap;
}

Texture::Format Texture::GetFormat() {
    return m_eFormat;
}

unsigned int Texture::GetWidth() {
    return m_nTextureWidth;
}

unsigned int Texture::GetHeight() {
    return m_nTextureHeight;
}
