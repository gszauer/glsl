#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Obj.h"
#include "Shader.h"
#include "Camera.h"
#include <sstream>
#include <fstream>
#include <ostream>

int GetWidth() { return 800; }
int GetHeight() { return 600; }

int g_nBlurRange = 15;
glm::vec3 camPosition = glm::vec3(5.0f, 2.0f, -10.0f);
Obj* obj;
Texture* diffuse;

Shader* shader;
Camera* camera;

Shader* blurShader;
Shader* dofShader;

PostProcess* blurFilter;
PostProcess* dofFilter;

bool g_bRenderFromCamer = false;
bool g_bRenderBlurBuffer = false;

bool Initialize() {
    std::cout << "Executable path: " << GetEecutablePath() << "\n";
    obj = new Obj("Assets/Crate.obj");
    shader = new Shader();
    shader->SetVertexShaderFromFile("Shaders/diffuse.vs");
    shader->SetFragmentShaderFromFile("Shaders/diffuse.fs");
    shader->Prepare();
    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 100.0);

    blurShader = new Shader("#define UNROLL_LOOP\n");
    blurShader->SetVertexShaderFromFile("Shaders/blur.vs");
    blurShader->SetFragmentShaderFromFile("Shaders/blur.fs");
    blurShader->Prepare();

    diffuse = new Texture("Assets/Crate.png");

    dofShader = new Shader();
    dofShader->SetVertexShaderFromFile("Shaders/blur.vs");
    dofShader->SetFragmentShaderFromFile("Shaders/dof.fs");
    dofShader->Prepare();

    blurFilter = new PostProcess(GetWidth(), GetHeight(), camera->GetRenderTarget(), blurShader);
    dofFilter = new PostProcess(GetWidth(), GetHeight(), camera->GetRenderTarget(), dofShader);

    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
    camera->LookAt(camPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 100.0f);
    camera->UpdateFrameBuffer(w, h);
    blurFilter->Resize(w, h);
    blurFilter->SetInput(camera->GetRenderTarget());
    dofFilter->Resize(w, h);
    dofFilter->SetInput(camera->GetRenderTarget());
    PrintGLError();
}

void Render() {
    camera->Bind();

    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);

    shader->Bind();

    shader->Mat4("view", camera->GetView());
    shader->Mat4("projection", camera->GetProjection());

    shader->Mat4("model", glm::translate(glm::vec3(0.0f, 0.0f, 0.0f)));
    shader->Texture("diffuse", *diffuse);
    obj->Render(shader->GetAttribLocation("position"), -1, shader->GetAttribLocation("uv"));

    shader->Mat4("model", glm::translate(glm::vec3(0.0f, 2.0f, 12.0f)));
    shader->Texture("diffuse", *diffuse);
    obj->Render(shader->GetAttribLocation("position"), -1, shader->GetAttribLocation("uv"));

    shader->Mat4("model", glm::translate(glm::vec3(0.0f, 1.0f, 6.0f)));
    shader->Texture("diffuse", *diffuse);
    PrintGLError();
    obj->Render(shader->GetAttribLocation("position"), -1, shader->GetAttribLocation("uv"));

    shader->Mat4("model", glm::translate(glm::vec3(0.0f, 2.0f, -8.0f)));
    shader->Texture("diffuse", *diffuse);
    obj->Render(shader->GetAttribLocation("position"), -1, shader->GetAttribLocation("uv"));

    shader->Unbind();
    camera->Unbind();

    if (g_bRenderFromCamer && !g_bRenderBlurBuffer) {
        camera->BlitToScreen(false);
    }
    else {
        blurFilter->Bind();
        blurFilter->GetShader()->Vec2("invViewport", glm::vec2(
                1.0f / float(blurFilter->GetResult()->GetWidth()),
                1.0f / float(blurFilter->GetResult()->GetHeight())
            )
       );
        blurFilter->Execute();
        blurFilter->Unbind();

        dofFilter->Bind();
        dofFilter->GetShader()->Texture("blur", *blurFilter->GetResult());
        dofFilter->GetShader()->Texture("depthBuffer", *camera->GetDepthTarget());
        dofFilter->GetShader()->Vec2("clipPlanes", glm::vec2(camera->GetNear(), camera->GetFar()));
        dofFilter->GetShader()->Float("blurDistance", 25);
        dofFilter->GetShader()->Float("blurRange", g_nBlurRange);
        dofFilter->GetShader()->Bool("debug", g_bRenderBlurBuffer);
        dofFilter->Execute();
        dofFilter->Unbind();
        dofFilter->BlitToScreen();
    }
}

bool Update(float dt) {
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete blurFilter;
    delete dofShader;
    delete blurShader;
    delete dofFilter;
    delete obj;
    delete shader;
    delete camera;
    delete diffuse;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        //camera->SaveToPng();
        ///blurFilter->SaveToPng();
        dofFilter->SaveToPng();
    }
    else if (key == 30) {
        g_bRenderFromCamer = true;
    }
    else if (key == 31) {
        g_bRenderFromCamer = false;
    }
    else if (key == 32) {
        g_bRenderBlurBuffer = !g_bRenderBlurBuffer;
    }
    else if (key == 21) {
        delete dofShader;
        dofShader = new Shader();
        dofShader->SetVertexShaderFromFile("Shaders/blur.vs");
        dofShader->SetFragmentShaderFromFile("Shaders/dof.fs");
        dofShader->Prepare();
        dofFilter->SetShader(dofShader);
        std::cout << "Reloaded shader\n";
    }
    else if (key == 20) {
        g_nBlurRange = 15.0f;
    }
    else if (key == 26) {
        g_nBlurRange = 10.0f;
    }
    else if (key != 41) {
        std::cout << "Key pressed: " << key << "\n";
    }
}
