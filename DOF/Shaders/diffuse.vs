#version 120
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

attribute vec3 uv;
attribute vec3 position;

varying vec2 texCoord;

void main() {
    texCoord = uv.xy;
    gl_Position = projection * view * model * vec4(position, 1.0f);
}