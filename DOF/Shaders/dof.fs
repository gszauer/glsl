#version 120
// ACTUAL BLEND: http://digitalerr0r.wordpress.com/2009/05/16/xna-shader-programming-tutorial-20-depth-of-field/

varying vec2 texCoord;

uniform sampler2D texture;
uniform sampler2D blur;
uniform sampler2D depthBuffer;

uniform bool debug;

// Blur distance and blur range are in frustum space. 
// That is, the max range is 0 to length
// The max distance is from frustum min to frustum max
uniform float blurDistance;
uniform float blurRange;
uniform vec2 clipPlanes;

float unpackDepth(vec4 depthSample) {
	return depthSample.x * 255.0 / 256.0 +
		depthSample.y * 255.0 / 65536.0 +
		depthSample.z * 255.0 / 16777216.0;
}

float linearizeDepth(float z) {
	float n = clipPlanes.x;
    float f = clipPlanes.y;
    return (2.0 * n) / (f + n - z * (f - n));
}

void main() {
    float z = linearizeDepth(unpackDepth(texture2D(depthBuffer, texCoord)));
	
	float Distance = clamp(blurDistance, clipPlanes.x, clipPlanes.y) / (clipPlanes.y - clipPlanes.x);
    float Range = clamp(blurRange, 0, clipPlanes.y - clipPlanes.x) / (clipPlanes.y - clipPlanes.x);
	
	float blurFactor =clamp(abs(z - Distance)/Range, 0, 1);

	if (debug)
		gl_FragColor = vec4(blurFactor, blurFactor, blurFactor, 1);
	else		
		gl_FragColor = mix(texture2D(texture, texCoord), texture2D(blur, texCoord), blurFactor);
}