#version 120

struct Light {
	vec4 position;	// x, y, z, directional radius
	vec4 color;		// r, g, b
	vec4 specular;	// r, g, b, spec power
};

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform Light lights[4];
uniform vec3 ambientColor;
uniform int numLights;

vec3 DirectionalLight(vec3 vertWorldNormal, vec3 lightWorldDir, vec3 lightColor, vec3 cameraDir, float specPower, vec3 specColor) {
	vec3 N = vertWorldNormal;
	vec3 L = lightWorldDir;
	float NdotL = max(dot(N, L), 0.0);
	vec3 diffuseColor = lightColor * NdotL;
	
	vec3 specularColor = vec3(0.0);
	if (specPower > 0.0) {
		vec3 reflection = normalize(vertWorldNormal + lightWorldDir * -0.5);
	    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);
	    specularColor = specColor * spec;
    }

	return ambientColor + diffuseColor + specularColor;
}

vec3 PointLight(vec3 vertWorldNormal, vec3 vertWorldPosition, vec3 lightWorldPos, vec3 lightColor, float radius, vec3 cameraDir, float specPower, vec3 specColor) {
	vec3 N = vertWorldNormal;
	vec3 L = normalize(lightWorldPos - vertWorldPosition);
	float NdotL = max(dot(N, L), 0.0);
	
	float distance = length(lightWorldPos - vertWorldPosition);
	float attenuation = 1.0 - (pow(distance, 2) / pow(radius, 2.0));
	
	vec3 diffuseColor = lightColor * NdotL * attenuation;

	vec3 specularColor = vec3(0.0);
	if (specPower > 0.0) {
		vec3 reflection = normalize(vertWorldNormal + L * -0.5);
	    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);
	    specularColor = specColor * spec * attenuation;
    }

    return ambientColor + diffuseColor + specularColor;
}

// Lighting data input
varying vec3 lightWorldDirection[4];
varying vec3 cameraDirection;
varying vec3 worldSpaceVertex;
varying vec2 texCoord;
varying mat3 tbnMatrix;

vec3 ProcessLight(int lightIndex, vec3 norm) {
	vec3 color = vec3(0);
	

	if (lights[lightIndex].position.w == 0) { // No radius, assume a point light!
		color = DirectionalLight(norm, lightWorldDirection[lightIndex], lights[lightIndex].color.xyz, cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz);
	}
	else {
		color = PointLight(norm, worldSpaceVertex, lights[lightIndex].position.xyz, lights[lightIndex].color.xyz, lights[lightIndex].position.w, cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz);
	}
	return color;
}

void main() {
	vec2 sample = texCoord.xy;

	vec3 litColor = vec3(0.0);
	vec3 norm = normalize(tbnMatrix * ((255.0 / 128.0) * texture2D(normalMap, sample).xyz - 1.0));

	if (numLights >= 1)
		litColor += ProcessLight(0, norm);
	if (numLights >= 2)
		litColor += ProcessLight(1, norm);
	if (numLights >= 3)
		litColor += ProcessLight(2, norm);
	if (numLights >= 4)
		litColor += ProcessLight(3, norm);

	gl_FragColor = vec4(litColor * vec3(texture2D(diffuseMap, sample)), 1.0);
}