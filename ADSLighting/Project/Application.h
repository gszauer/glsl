#ifndef _H_APPLICATION_
#define _H_APPLICATION_

bool Initialize();
void Resize(int w, int h);
void Render();
bool Update(float dt);
void Shutdown();
void OnKeyDown(unsigned int key, float dt);
void DebugDrawImage(unsigned int imageId);

#endif