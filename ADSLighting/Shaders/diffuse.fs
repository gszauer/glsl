#version 120

struct Light {
	vec4 position;	// x, y, z, directional radius
	vec4 color;		// r, g, b
	vec4 specular;	// r, g, b, spec power
};

uniform mat4 model;
uniform Light lights[4];
uniform vec3 ambientColor;
uniform int numLights;

vec3 DirectionalLightAmbient() {
	return ambientColor;
}

vec3 DirectionalLightDiffuse(vec3 vertexWorldNormal, vec3 lightWorldDirection, vec3 lightColor) {
	vec3 N = vertexWorldNormal;
	vec3 L = lightWorldDirection;
	float NdotL = max(dot(N, L), 0.0);

	return lightColor * NdotL;
}

vec3 DirectionalLightSpecular(vec3 vertexWorldNormal, vec3 lightWorldDirection, vec3 cameraDir, float specPower, vec3 specColor) {
	if (specPower == 0.0)
		return vec3(0.0);
	// Reflection is expensive, use halfway vector!
	//vec3 reflection = reflect(-lightWorldDirection, vertexWorldNormal);
	vec3 reflection = normalize(vertexWorldNormal + lightWorldDirection * -0.5);

    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);

	return normalize(specColor) * vec3(spec);
}

vec3 PointLightAmbient() {
	return ambientColor;
}

vec3 PointLightDiffuse(vec3 vertexWorldNormal, vec3 vertexWorldPosition, vec3 lightWorldPosition, vec3 lightColor, float radius) {
	vec3 N = vertexWorldNormal;
	vec3 L = normalize(lightWorldPosition - vertexWorldPosition);
	float NdotL = max(dot(N, L), 0.0);

	float distance = length(lightWorldPosition - vertexWorldPosition);
	float attenuation = 1.0 - (pow(distance, 2) / pow(radius, 2.0));

	return lightColor * NdotL * attenuation;
}

vec3 PointLightSpecular(vec3 vertexWorldNormal, vec3 vertexWorldPosition, vec3 lightWorldPosition, vec3 cameraDir, float specPower, vec3 specColor, float radius) {
	if (specPower == 0.0)
		return vec3(0.0);
	vec3 lightDirection = normalize(lightWorldPosition - vertexWorldPosition);
	vec3 reflection = normalize(vertexWorldNormal + lightDirection * -0.5);

    float spec = pow( max( 0.0, dot(cameraDir, reflection)), specPower);

    float distance = length(lightWorldPosition - vertexWorldPosition);
	float attenuation = 1.0 - (pow(distance, 2) / pow(radius, 2.0));

	return normalize(specColor) * vec3(spec) * attenuation;
}

// Lighting data input
varying vec3 lightWorldDirection[4];
varying vec3 vertexWorldNormal[4];
varying vec3 cameraDirection;
varying vec3 worldSpaceVertex;

vec3 ProcessLight(int lightIndex) {
	if (lights[lightIndex].position.w == 0) {
		return DirectionalLightAmbient() + 
			DirectionalLightDiffuse(vertexWorldNormal[lightIndex], lightWorldDirection[lightIndex], lights[lightIndex].color.xyz) +
			DirectionalLightSpecular(vertexWorldNormal[lightIndex], lightWorldDirection[lightIndex], cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz);
	}
	
	return PointLightAmbient() + 
	PointLightDiffuse(vertexWorldNormal[lightIndex], worldSpaceVertex, lights[lightIndex].position.xyz, lights[lightIndex].color.xyz, lights[lightIndex].position.w) +
	PointLightSpecular(vertexWorldNormal[lightIndex], worldSpaceVertex, lights[lightIndex].position.xyz, cameraDirection, lights[lightIndex].specular.w, lights[lightIndex].specular.xyz, lights[lightIndex].position.w);
}

void main() {
	vec3 litColor = vec3(0.0);

	if (numLights >= 1)
		litColor += ProcessLight(0);
	if (numLights >= 2)
		litColor += ProcessLight(1);
	if (numLights >= 3)
		litColor += ProcessLight(2);
	if (numLights >= 4)
		litColor += ProcessLight(3);

	gl_FragColor = vec4(litColor, 1.0);
}