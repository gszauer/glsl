#version 120

struct Light {
	vec4 position;	// x, y, z, directional radius
	vec4 color;		// r, g, b
	vec4 specular;	// r, g, b, spec power
};

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform Light lights[4];

attribute vec3 position;
attribute vec3 normal;

// Lighting data output
varying vec3 lightWorldDirection[4];
varying vec3 vertexWorldNormal[4];
varying vec3 cameraDirection;
varying vec3 worldSpaceVertex;

void main() {
	mat3 normalMatrix = mat3(model);
	vec4 vertex = projection * view * model * vec4(position, 1.0);
	vec3 eyePosition = vec3(view[3][0], view[3][1], view[3][2]);

    vertexWorldNormal[0] = normalize(normalMatrix * normal);
    lightWorldDirection[0] = normalize(lights[0].position.xyz); 
    cameraDirection = normalize(eyePosition - vertex.xyz);
    worldSpaceVertex = vec3(model * vec4(position, 1.0));
    
    gl_Position = vertex;
}