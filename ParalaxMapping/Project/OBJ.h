//-----------------------------------------------------------------------------
// Copyright (c) 2007 dhpoware. All Rights Reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#if !defined(MODEL_OBJ_H)
#define MODEL_OBJ_H

#include <cstdio>
#include <map>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Alias|Wavefront OBJ file loader.
//
// This OBJ file loader contains the following restrictions:
// 1. Group information is ignored. Faces are grouped based on the material
//    that each face uses.
// 2. Object information is ignored. This loader will merge everything into a
//    single object.
// 3. The MTL file must be located in the same directory as the OBJ file. If
//    it isn't then the MTL file will fail to load and a default material is
//    used instead.
// 4. This loader triangulates all polygonal faces during importing.
//-----------------------------------------------------------------------------

class OBJ
{
public:
    struct Vertex
    {
        float position[3];
        float texCoord[2];
        float normal[3];
        float tangent[4];
        float bitangent[3];
    };

    OBJ(const char* path);
    ~OBJ();
    void Render(int position, int normal = -1, int uv = -1, int tan = -1);

    const std::string &getPath() const;

    bool hasNormals() const;
    bool hasPositions() const;
    bool hasTangents() const;
    bool hasTextureCoords() const;
private:
    void MakeOGLObjects();
    void DestroyOGLObjects();

    const Vertex &getVertex(int i) const;
    const Vertex *getVertexBuffer() const;
    int getVertexSize() const;
    const unsigned int *getIndexBuffer() const;
    int getIndexSize() const;

    int getNumberOfIndices() const;
    int getNumberOfTriangles() const;
    int getNumberOfVertices() const;

    OBJ();
    bool import(const char *pszFilename, bool rebuildNormals = false);
    void destroy();
    void reverseWinding();

    void addTrianglePos(int index, int material, int v0, int v1, int v2);
    void addTrianglePosNormal(int index, int material, int v0, int v1, int v2, int vn0, int vn1, int vn2);
    void addTrianglePosTexCoord(int index, int material, int v0, int v1, int v2, int vt0, int vt1, int vt2);
    void addTrianglePosTexCoordNormal(int index, int material, int v0, int v1, int v2, int vt0, int vt1, int vt2, int vn0, int vn1, int vn2);
    int addVertex(int hash, const Vertex *pVertex);
    void generateNormals();
    void generateTangents();
    void importGeometryFirstPass(FILE *pFile);
    void importGeometrySecondPass(FILE *pFile);

    bool m_hasPositions;
    bool m_hasTextureCoords;
    bool m_hasNormals;
    bool m_hasTangents;

    int m_numberOfVertexCoords;
    int m_numberOfTextureCoords;
    int m_numberOfNormals;
    int m_numberOfTriangles;

    std::string m_directoryPath;

    std::vector<Vertex> m_vertexBuffer;
    std::vector<unsigned int> m_indexBuffer;
    std::vector<float> m_vertexCoords;
    std::vector<float> m_textureCoords;
    std::vector<float> m_normals;

    unsigned int m_glIndexBufferObject;
    unsigned int m_glVertexBufferObject;
    unsigned int m_glNumIndices;

    std::map<int, std::vector<int> > m_vertexCache;
};

//-----------------------------------------------------------------------------
inline const unsigned int *OBJ::getIndexBuffer() const
{ return &m_indexBuffer[0]; }

inline int OBJ::getIndexSize() const
{ return static_cast<int>(sizeof(int)); }

inline int OBJ::getNumberOfIndices() const
{ return m_numberOfTriangles * 3; }

inline int OBJ::getNumberOfTriangles() const
{ return m_numberOfTriangles; }

inline int OBJ::getNumberOfVertices() const
{ return static_cast<int>(m_vertexBuffer.size()); }

inline const std::string &OBJ::getPath() const
{ return m_directoryPath; }

inline const OBJ::Vertex &OBJ::getVertex(int i) const
{ return m_vertexBuffer[i]; }

inline const OBJ::Vertex *OBJ::getVertexBuffer() const
{ return &m_vertexBuffer[0]; }

inline int OBJ::getVertexSize() const
{ return static_cast<int>(sizeof(Vertex)); }

inline bool OBJ::hasNormals() const
{ return m_hasNormals; }

inline bool OBJ::hasPositions() const
{ return m_hasPositions; }

inline bool OBJ::hasTangents() const
{ return m_hasTangents; }

inline bool OBJ::hasTextureCoords() const
{ return m_hasTextureCoords; }

#endif
