#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Shader.h"
#include "Camera.h"
#include "OBJ.h"
#include <sstream>

// specularColor.xyz = Color info
// specularColor.w = specular power
// diffuseColor.xyz = Color info
// diffuseColor.w = unused!
// globalPosition.xyz = SpatialPosition
// globalPosition.w = radius

#define TO_RAD              0.0174532925f
#define TRANSLATE_SPEED     5.0f
#define PRINT_VEC(vec)        ": " << vec.x << ", " << vec.y << ", " << vec.z

int GetWidth() { return 800; }
int GetHeight() { return 600; }

glm::vec3 cameraPosition;
glm::vec2 cameraAngle;

glm::vec3 forward;
glm::vec3 right;
glm::vec3 up;

Camera* camera;
int g_nLightStatus = 2;
float objRotation = 0.0f;

struct PointLight {
    glm::vec4 globalPosition;
    glm::vec4 diffuseColor;
    glm::vec4 specularColor;

    Shader* rs;
    OBJ* vs;

    PointLight() {
        rs = new Shader();
        rs->SetVertexShaderFromFile("Shaders/light_visualizer.vs");
        rs->SetFragmentShaderFromFile("Shaders/light_visualizer.fs");
        rs->Prepare();

        vs = new OBJ("Assets/Sphere.obj");
    }

    ~PointLight() {
        delete rs;
        delete vs;
    }

    void Render() { // Pos * Rot * Scale
        if (g_nLightStatus == 2) return;

        if (g_nLightStatus == 1) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glBlendEquation(GL_FUNC_ADD);
        }

        rs->Bind();
        rs->Mat4("view", camera->GetView());
        rs->Mat4("projection", camera->GetProjection());
        glm::vec3 gPosition = glm::vec3(globalPosition.x, globalPosition.y, globalPosition.z);
        rs->Mat4("model", glm::translate(gPosition) * glm::scale(glm::vec3(globalPosition.w)));
        rs->Vec4("color", diffuseColor);
        vs->Render(rs->GetAttribLocation("position"));
        rs->Unbind();

        if (g_nLightStatus == 1)
            glDisable(GL_BLEND);
    }
};

glm::vec3 ambientLight = glm::vec3(0.1f);
glm::vec3 camPosition = glm::vec3(0.0f, 0.0f, -10.0f);
PointLight* light1;
PointLight* light2;
PointLight* light3;

OBJ* model;
OBJ* plane;
Shader* diffuseShader;
Texture* bricks;
Texture* bricks_n;
Texture* bricks_d;

float dispScale;
float dispOffset;

bool Initialize() {
    forward = glm::vec3(0.0f, 0.0f, 1.0f);
    up = glm::vec3(0.0f, 1.0f, 0.0f);
    right = glm::vec3(1.0f, 0.0f, 0.0f);

    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 500.0f);
    cameraPosition = glm::vec3(0.0f, 3.0f, -10.0f);
    cameraAngle = glm::vec2(0.0f, 0.0f);

    dispScale = 0.03f;
    dispOffset = -1.0f;

    light1 = new PointLight();
    light2 = new PointLight();
    light3 = new PointLight();

    light1->diffuseColor = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f); // r, g, b, unused
    light1->specularColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f); // r, g, b, spec power
    light1->globalPosition = glm::vec4(1.5f, 1.0f, 0.0f, 5.0f); // x, y, z, radius

    model = new OBJ("Assets/plane.obj");
    plane = new OBJ("Assets/plane.obj");

    bricks = new Texture("Assets/bricks2.jpg", Texture::REPEAT);
    bricks_n = new Texture("Assets/bricks2_normal.jpg", Texture::REPEAT);
    bricks_d = new Texture("Assets/bricks2_disp.jpg", Texture::REPEAT);

    diffuseShader = new Shader();
    diffuseShader->SetVertexShaderFromFile("Shaders/diffuse.vs");
    diffuseShader->SetFragmentShaderFromFile("Shaders/diffuse.fs");
    diffuseShader->Prepare();

    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 500.0f);
    camera->UpdateFrameBuffer(w, h);
    PrintGLError();
}

void ImLine(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& c) {
    glBegin(GL_LINES);
    glColor3f(c.x, c.y, c.z);
    glVertex3f(p1.x, p1.y, p1.z);
    glColor3f(c.x, c.y, c.z);
    glVertex3f(p2.x, p2.y, p2.z);
    glEnd();
}

void Render() {
    glm::quat rotation = glm::quat(glm::vec3(cameraAngle.y * TORAD, cameraAngle.x * TORAD, 0.0f));
    camera->LookAt(cameraPosition, cameraPosition + glm::normalize(forward * rotation), glm::normalize(up * rotation));

    camera->Bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);

    diffuseShader->Bind();
    diffuseShader->Mat4("view", camera->GetView());
    diffuseShader->Mat4("projection", camera->GetProjection());

    rotation = glm::quat(glm::vec3(0.0f, objRotation * TORAD, 0.0f));
    diffuseShader->Mat4("model", glm::mat4_cast(rotation));

    diffuseShader->Int("numLights", 1);
    diffuseShader->Vec3("ambientColor", ambientLight);
    diffuseShader->Vec4 ("lights[0].position", light1->globalPosition);
    diffuseShader->Vec4 ("lights[0].color", light1->diffuseColor);
    diffuseShader->Vec4 ("lights[0].specular", light1->specularColor);

    diffuseShader->Mat4("model", glm::translate(glm::vec3(0.0f, -1.0f, 0.0f)));
    diffuseShader->Texture("diffuseMap", *bricks);
    diffuseShader->Texture("normalMap", *bricks_n);
    diffuseShader->Texture("displacementMap", *bricks_d);

    diffuseShader->Float("displacementScale", dispScale);
    float bias = dispScale / 2.0f;
    diffuseShader->Float("displacementBias", (-bias) + (bias * dispOffset));

    if (!diffuseShader->ContainsAttribute("_position"))
        std::cout << "Shader does not define _position attribute\n";
    if (!diffuseShader->ContainsAttribute("_normal"))
        std::cout << "Shader does not define _normal attribtue\n";
    if (!diffuseShader->ContainsAttribute("_uv"))
        std::cout << "Shader does not define _uv attribute\n";
    if (!diffuseShader->ContainsAttribute("_tangent"))
        std::cout << "Shader does not define _tangent attribute\n";
    assert(diffuseShader->ContainsAttribute("_position"));
    assert(diffuseShader->ContainsAttribute("_normal"));
    assert(diffuseShader->ContainsAttribute("_uv"));
    assert(diffuseShader->ContainsAttribute("_tangent"));
    plane->Render(diffuseShader->GetAttribLocation("_position"), diffuseShader->GetAttribLocation("_normal"), diffuseShader->GetAttribLocation("_uv"), diffuseShader->GetAttribLocation("_tangent"));

    diffuseShader->Mat4("model", glm::translate(glm::vec3(2.0f, 2.0f, 2.0f)) * glm::mat4_cast(glm::quat(glm::vec3(-90.0f * TO_RAD, 0.0f, 0.0f))));

    //plane->Render(diffuseShader->GetAttribLocation("_position"), diffuseShader->GetAttribLocation("_normal"), diffuseShader->GetAttribLocation("_uv"), diffuseShader->GetAttribLocation("_tangent"));

    diffuseShader->Unbind();

    PrintGLError();
    light1->Render();
    PrintGLError();

    camera->Unbind();

    camera->BlitToScreen(false);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(glm::value_ptr(camera->GetProjection()));
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(glm::value_ptr(camera->GetView()));
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);
    ImLine(glm::vec3(0), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    ImLine(glm::vec3(0), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    ImLine(glm::vec3(0), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    glm::vec3 gPos = glm::vec3(light1->globalPosition.x, light1->globalPosition.y, light1->globalPosition.z);
    ImLine(glm::vec3(0), gPos, glm::vec3(1.0f, 1.0f, 0.0f));
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

bool Update(float dt) {
    objRotation += dt * 36.0f;
    while (objRotation > 360.0f)
        objRotation -= 360.0f;
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete light1;
    delete light2;
    delete light3;
    delete camera;
    delete model;
    delete plane;
    delete bricks;
    delete bricks_n;
    delete bricks_d;
    delete diffuseShader;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        camera->SaveToPng();
    }
    else if (key == 30) {
        if (++g_nLightStatus > 2)
            g_nLightStatus = 0;
    }
    else if (key == 31) {
        camPosition = glm::vec3(0.0f, 0.0f, -10.0f);
    }
    else if (key != 41 && key != 4 && key != 7 && key != 22 && key != 26 &&
             key != 12 && key != 13 && key != 14 && key != 15) {
        std::cout << "Key pressed: " << key << "\n";
    }

    { // FREE CAMERA
        if (key == 14) { // K
            cameraAngle.y -= 36.0f * dt;
            if (cameraAngle.y < -60.0f)
                cameraAngle.y = -60.0f;
        }
        else if (key == 13) { // J
            cameraAngle.x -= 36.0f * dt;
            while (cameraAngle.x < 0.0f)
                cameraAngle += 360.0f;
        }
        else if (key == 12) { // I
            cameraAngle.y += 36.0f * dt;
            if (cameraAngle.y > 60.0f)
                cameraAngle.y = 60.0f;
        }
        else if (key == 15) { // L
            cameraAngle.x += 36.0f * dt;
            while (cameraAngle.x > 360.0f)
                cameraAngle -= 360.0f;
        }

        glm::quat rotation = glm::quat(glm::vec3(cameraAngle.y * TORAD, cameraAngle.x * TORAD, 0.0f));

        if (key == 4) { // A
            cameraPosition += (right * rotation) * TRANSLATE_SPEED * dt;
        }
        else if (key == 7) { // D
            cameraPosition -= (right * rotation) * TRANSLATE_SPEED * dt;
        }
        if (key == 22) { // S
            cameraPosition -= (forward * rotation) * TRANSLATE_SPEED * dt;
        }
        else if (key == 26) { // W
            cameraPosition += (forward * rotation) * TRANSLATE_SPEED * dt;
        }
    }

}
