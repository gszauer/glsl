#version 120

varying vec2 texCoord;

uniform sampler2D texture; // Camera
uniform sampler2D ssao;

void main() {
    vec4 diffuse = texture2D(texture, texCoord);
    vec4 ao = texture2D(ssao, texCoord);

    gl_FragColor = diffuse * ao;
}