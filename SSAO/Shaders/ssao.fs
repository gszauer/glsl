#version 120
// Based on: http://theorangeduck.com/page/pure-depth-ssao
varying vec2 texCoord;

uniform sampler2D texture; // depth!
uniform sampler2D random;
uniform vec2 clipPlanes;
uniform vec2 invViewport;

// SSAO specific
uniform float totalStrength;
uniform float base;
uniform float area;
uniform float falloff;
uniform float radius;

float LinearizeDepth(float z) {
    float n = clipPlanes.x;
    float f = clipPlanes.y;
    return (2.0 * n) / (f + n - z * (f - n));
}

float UnpackDepth(vec4 depthSample) {
    return depthSample.x * 255.0 / 256.0 +
    depthSample.y * 255.0 / 65536.0 +
    depthSample.z * 255.0 / 16777216.0;
}

vec3 NormalFromDepth(float depth, vec2 texcoords) {
    vec2 offset1 = vec2(0.0, invViewport.y); // 1 * (1 / screenH) - ie 1 pixel jump
    vec2 offset2 = vec2(invViewport.x, 0.0); 
    
    float depth1 = LinearizeDepth(UnpackDepth(texture2D(texture, texcoords + offset1)));
    float depth2 = LinearizeDepth(UnpackDepth(texture2D(texture, texcoords + offset2)));
    
    vec3 p1 = vec3(offset1, depth1 - depth);
    vec3 p2 = vec3(offset2, depth2 - depth);
    
    vec3 normal = cross(p1, p2);
    if (1.0 - depth > 0.001)
        normal.z = normal.z * -1.0;
    
    return normalize(normal);
}

void main() {
    const int samples = 16;
    vec3 sample_sphere[samples] = vec3[samples](
        vec3( 0.5381, 0.1856,-0.4319), vec3( 0.1379, 0.2486, 0.4430),
        vec3( 0.3371, 0.5679,-0.0057), vec3(-0.6999,-0.0451,-0.0019),
        vec3( 0.0689,-0.1598,-0.8547), vec3( 0.0560, 0.0069,-0.1843),
        vec3(-0.0146, 0.1402, 0.0762), vec3( 0.0100,-0.1924,-0.0344),
        vec3(-0.3577,-0.5301,-0.4358), vec3(-0.3169, 0.1063, 0.0158),
        vec3( 0.0103,-0.5869, 0.0046), vec3(-0.0897,-0.4940, 0.3287),
        vec3( 0.7119,-0.0154,-0.0918), vec3(-0.0533, 0.0596,-0.5411),
        vec3( 0.0352,-0.0631, 0.5460), vec3(-0.4776, 0.2847,-0.0271)
    );

    vec3 rnd = normalize(texture2D(random, texCoord).rgb);
    float depth = LinearizeDepth(UnpackDepth(texture2D(texture, texCoord)));
    vec3 pos = vec3(texCoord, depth);
    vec3 normal = NormalFromDepth(depth, texCoord);

    float rad_depth = radius / depth;
    float occlusion = 0.0;
    for (int i = 0; i < samples; ++i) {
        vec3 ray = rad_depth * reflect(sample_sphere[i], rnd);
        vec3 hemi_ray = pos + sign(dot(ray, normal)) * ray;

        float occDepth = LinearizeDepth(UnpackDepth(texture2D(texture, clamp(hemi_ray.xy, 0, 1))));
        float diff = depth - occDepth;

        occlusion += step(falloff, diff) * (1.0 - smoothstep(falloff, area, diff));
    }

    float ao = 1.0 - totalStrength * occlusion * (1.0 / samples);
    gl_FragColor = vec4(clamp(ao + base, 0, 1),clamp(ao + base, 0, 1),clamp(ao + base, 0, 1),1);
}