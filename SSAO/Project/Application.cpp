#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Obj.h"
#include "Shader.h"
#include "Camera.h"
#include <sstream>

int GetWidth() { return 800; }
int GetHeight() { return 600; }

glm::vec3 camPosition = glm::vec3(0.0f, 4.0f, 5.0f);

Obj* obj;
Shader* shader;
Texture* diffuse;

Shader* ssaoShader;
PostProcess* ssaoFilter;

Shader* combineShader;
PostProcess* finalCombiner;

Camera* camera;
Texture* randTex;

bool g_bRenderFromCamer = false;
bool g_bRenderSSAO = false;

bool Initialize() {
    obj = new Obj("Assets/Deathstroke.obj");
    diffuse = new Texture("Assets/Deathstroke_D.png");
    randTex = new Texture("Assets/random.png", Texture::REPEAT);
    shader = new Shader();
    shader->SetVertexShaderFromFile("Shaders/diffuse.vs");
    shader->SetFragmentShaderFromFile("Shaders/diffuse.fs");
    shader->Prepare();
    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 100.0f);
    ssaoShader = new Shader();
    ssaoShader->SetVertexShaderFromFile("Shaders/ssao.vs");
    ssaoShader->SetFragmentShaderFromFile("Shaders/ssao.fs");
    ssaoShader->Prepare();
    ssaoFilter = new PostProcess(GetWidth(), GetHeight(), camera->GetDepthTarget(), ssaoShader);
    combineShader = new Shader();
    combineShader->SetVertexShaderFromFile("Shaders/ssao.vs");
    combineShader->SetFragmentShaderFromFile("Shaders/combiner.fs");
    combineShader->Prepare();
    finalCombiner = new PostProcess(GetWidth(), GetHeight(), camera->GetRenderTarget(), combineShader);
    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
    camera->LookAt(camPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 100.0f);
    camera->UpdateFrameBuffer(w, h);
    ssaoFilter->Resize(w, h);
    ssaoFilter->SetInput(camera->GetDepthTarget());
    finalCombiner->Resize(w, h);
    finalCombiner->SetInput(camera->GetRenderTarget());
    PrintGLError();
}

void Render() {
    camera->Bind();

    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);

    shader->Bind();

    shader->Mat4("view", camera->GetView());
    shader->Mat4("projection", camera->GetProjection());
    shader->Mat4("model", glm::translate(glm::vec3(-1.0f, -1.0f, 2.0f)));
    shader->Texture("diffuse", *diffuse);
    obj->Render(shader->GetAttribLocation("position"), -1, shader->GetAttribLocation("uv"));

    shader->Unbind();
    camera->Unbind();

    if (g_bRenderFromCamer && !g_bRenderSSAO) {
        camera->BlitToScreen(false);
    }
    else {
        ssaoFilter->Bind();
        ssaoFilter->GetShader()->Vec2("clipPlanes", glm::vec2(camera->GetNear(), camera->GetFar()));
        ssaoFilter->GetShader()->Vec2("invViewport", glm::vec2(1.0f / ssaoFilter->GetResult()->GetWidth(), 1.0f / ssaoFilter->GetResult()->GetHeight()));
        ssaoFilter->GetShader()->Float("totalStrength", 1.0f);
        ssaoFilter->GetShader()->Float("base", 0.2f);
        ssaoFilter->GetShader()->Float("area", 0.0075f);
        ssaoFilter->GetShader()->Float("falloff", 0.000001f);
        ssaoFilter->GetShader()->Float("radius", 0.0002f);
        ssaoFilter->GetShader()->Texture("random", *randTex);
        ssaoFilter->Execute();
        ssaoFilter->Unbind();
        if (g_bRenderSSAO)
            ssaoFilter->BlitToScreen();

        finalCombiner->Bind();
        finalCombiner->GetShader()->Texture("ssao", *ssaoFilter->GetResult());
        finalCombiner->Execute();
        finalCombiner->Unbind();
        if (!g_bRenderSSAO)
            finalCombiner->BlitToScreen();
    }
}

bool Update(float dt) {
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete ssaoFilter;
    delete ssaoShader;
    delete obj;
    delete shader;
    delete camera;
    delete diffuse;
    delete combineShader;
    delete randTex;
    delete finalCombiner;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        camera->SaveToPng();
        finalCombiner->SaveToPng();
        ssaoFilter->SaveToPng();
    }
    else if (key == 30) {
        g_bRenderFromCamer = true;
    }
    else if (key == 31) {
        g_bRenderFromCamer = false;
    }
    else if (key == 32) {
        g_bRenderSSAO = !g_bRenderSSAO;
    }
    else if (key == 21) {
        ssaoShader = new Shader();
        ssaoShader->SetVertexShaderFromFile("Shaders/ssao.vs");
        ssaoShader->SetFragmentShaderFromFile("Shaders/ssao.fs");
        ssaoShader->Prepare();
        ssaoFilter->SetShader(ssaoShader);
    }
    else if (key != 41) {
        std::cout << "Key pressed: " << key << "\n";
    }
}
