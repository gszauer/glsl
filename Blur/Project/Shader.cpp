#include "Shader.h"
#include <assert.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <istream>
#include <assert.h>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include "OGLHeaders.h"
#include "Texture.h"
#include <cstring>
#include <stdio.h>
#include <fstream>
#include <ostream>

//#define DEBUG_UNIFORMS
//#define DEBUG_ATTRIBUTES
//#define PRINT_SET_VARS

Shader::Shader() {
    m_nShaderHandle = 0;
	m_strCustomDefines = 0;
	m_strLastError = 0;
    m_nLastBoundTextureIndex = 0;
    m_nVertHandle = 0;
    m_nFragHandle = 0;
    m_nUniformCount = 0;
    m_nAttributeCount = 0;
}

Shader::Shader(const char* defines) {
    m_nShaderHandle = 0;
	m_strCustomDefines = 0;
	m_strLastError = 0;
    m_nLastBoundTextureIndex = 0;
    m_nVertHandle = 0;
    m_nFragHandle = 0;
    m_nUniformCount = 0;
    m_nAttributeCount = 0;

    SetDefines(defines);
}

Shader::Shader(const char* vertex, const char* fragment) {
    m_nShaderHandle = 0;
	m_strCustomDefines = 0;
	m_strLastError = 0;
    m_nLastBoundTextureIndex = 0;
    m_nVertHandle = 0;
    m_nFragHandle = 0;
    m_nUniformCount = 0;
    m_nAttributeCount = 0;

    SetVertexShader(vertex);
    SetFragmentShader(fragment);
}

Shader::Shader(const char* defines, const char* vertex, const char* fragment) {
    SetDefines(defines);
    SetVertexShader(vertex);
    SetFragmentShader(fragment);
    m_nUniformCount = 0;
    m_nAttributeCount = 0;
}

Shader::~Shader() {
    if (m_strCustomDefines != 0)
        delete[] m_strCustomDefines;
    m_strCustomDefines = 0;
    if (m_strLastError != 0)
        delete[] m_strLastError;
    if (m_nShaderHandle != 0)
        glDeleteProgram(m_nShaderHandle);
}

void Shader::SetDefines(const char* defines) {
    assert(m_strCustomDefines == 0);
    int len = (int)strlen(defines);
    m_strCustomDefines = new char[len+1];
    memset(m_strCustomDefines, 0, sizeof(char) * len);
    strcpy(m_strCustomDefines, defines);
    //std::cout << "Defines: " << m_strCustomDefines << "\n";
}

void Shader::SetError(const char* error) {
    int len = (int)strlen(error);
    m_strLastError = new char[len + 1];
    memset(m_strLastError, 0, sizeof(char) * len);
    strcpy(m_strLastError, error);
    std::cout << "Error: " << error << "\n";
}

void Shader::SetVertexShaderFromFile(const char* path) {
    m_nVertName = path;
    std::ifstream shaderFile(path, std::ifstream::in);
    if (!shaderFile) {
        char error[512];
        sprintf(error, "Couldn't load vertex shader: %s", path);
        SetError(error);
        assert(true);
    }

    std::string shaderString;
    shaderFile.seekg(0, std::ios::end);
    shaderString.reserve(shaderFile.tellg());
    shaderFile.seekg(0, std::ios::beg);
    shaderString.assign((std::istreambuf_iterator<char>(shaderFile)),
                        std::istreambuf_iterator<char>());
    shaderFile.close();

    SetVertexShader(shaderString.c_str());
}

void Shader::SetFragmentShaderFromFile(const char* path) {
    m_nFragName = path;
    std::ifstream shaderFile(path, std::ifstream::in);
    if (!shaderFile) {
        char error[512];
        sprintf(error, "Couldn't load vertex shader: %s", path);
        SetError(error);
        assert(true);
    }

    std::string shaderString;
    shaderFile.seekg(0, std::ios::end);
    shaderString.reserve(shaderFile.tellg());
    shaderFile.seekg(0, std::ios::beg);
    shaderString.assign((std::istreambuf_iterator<char>(shaderFile)),
                        std::istreambuf_iterator<char>());
    shaderFile.close();

    SetFragmentShader(shaderString.c_str());
}

void Shader::SetVertexShader(const char* shaderContent) {
    m_nVertHandle = CompileShader_Internal(shaderContent, GL_VERTEX_SHADER);
    assert (m_nVertHandle != -1);
}

void Shader::SetFragmentShader(const char* shaderContent) {
    m_nFragHandle = CompileShader_Internal(shaderContent, GL_FRAGMENT_SHADER);
    assert (m_nFragHandle != -1);
}

int Shader::CompileShader_Internal(const char* shaderContent, unsigned int type) {
    const char* version_start = strstr (shaderContent,"#version 120");
    if (version_start == 0) {
        std::cout << "Abort compiling ";
        if (type == GL_VERTEX_SHADER)
            std::cout << "Vertex (" << m_nVertName << ")";
        else if (type == GL_FRAGMENT_SHADER)
            std::cout << "Fragment (" << m_nFragName << ")";
        else
            std::cout << "???";
        std::cout << " shader, must include #version 120\n";
    }
    assert(version_start != 0);

    if (version_start != (char*)shaderContent) {
        std::cout << "Warning, ";
        if (type == GL_VERTEX_SHADER)
            std::cout << m_nVertName;
        else if (type == GL_FRAGMENT_SHADER)
            std::cout << m_nFragName;
        else
            std::cout << "???";
        std::cout << " should start with #version\n";
    }

    char* shader = (char*)shaderContent;
    if (m_strCustomDefines != 0) {
        int len = int(strlen(shaderContent));
        len += strlen(m_strCustomDefines);
        len += strlen("012345676120123s123s1n"); // #version 120\n ...

        shader = new char[len];
        memset(shader, 0, sizeof(char) * len);
        sprintf(shader, "#1234567 120\n%s\n%s", m_strCustomDefines, shaderContent);
        char* v_tag = (char*)strstr (shader,"#version");
        strncpy (v_tag,"//ersion",strlen("//ersion"));
        v_tag = (char*)strstr (shader,"#1234567");
        strncpy (v_tag,"#version",strlen("#version"));
#if DEBUG_SAVE
        std::string outPath = GetEecutablePath();
        if (type == GL_VERTEX_SHADER)
            outPath += m_nVertName.substr(m_nVertName.find_last_of('/') + 1);
        else if (type == GL_FRAGMENT_SHADER)
            outPath += m_nFragName.substr(m_nFragName.find_last_of('/') + 1);
        else
            outPath += "unknown.shader";
        std::cout << "saving shader to: " << outPath << "\n";
        std::ofstream outFile(outPath.c_str());
        outFile << shader;
        outFile.close();
#endif // DEBUG_SAVE
    }

    GLuint result = glCreateShader(type);
    GLint status;
    if (result == 0) {
        SetError("Couldn't create shader");
        return -1;
    }

    int shader_length = int(strlen(shader));
    glShaderSource(result, 1, (const GLchar**)&shader, &shader_length);
    glCompileShader(result);

    glGetShaderiv(result, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        glGetShaderiv(result, GL_INFO_LOG_LENGTH, &status);
        if (status > 0) {
            char* log = new char[status];
            glGetShaderInfoLog(result, status, &status, log);
            SetError(log);
            delete[] log;
        }
        else {
            SetError("Could not compile shader!");
        }
        if (m_strCustomDefines != 0)
            delete[] shader;
        std::cout << "Status was false\n";
        return -1;
    }


    if (m_strCustomDefines != 0)
        delete[] shader;
    return result;
}

bool Shader::Prepare() {
    assert(m_nVertHandle != 0);
    assert(m_nFragHandle != 0);

    m_nShaderHandle = glCreateProgram();
    GLint result;
    assert (m_nShaderHandle != 0);

    glAttachShader(m_nShaderHandle, m_nVertHandle);
    glAttachShader(m_nShaderHandle, m_nFragHandle);
    glLinkProgram(m_nShaderHandle);

    glGetProgramiv(m_nShaderHandle, GL_LINK_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Could not link shader program!\n";
        glGetProgramiv(m_nShaderHandle, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetProgramInfoLog(m_nShaderHandle, result, &result, log);
            SetError(log);
            delete[] log;
        }
        return false;
    }

    glDeleteShader(m_nVertHandle);
    glDeleteShader(m_nFragHandle);
    m_nVertHandle = 0;
    m_nFragHandle = 0;

    PopulateUniforms();
    PopulateAttributes();

    if (m_strCustomDefines != 0)
        delete[] m_strCustomDefines;
    m_strCustomDefines = 0;

    return true;
}

void Shader::PopulateUniforms() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    glUseProgram(m_nShaderHandle);
    GLint   uniforms, length;
    glGetProgramiv(m_nShaderHandle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &length);
    glGetProgramiv(m_nShaderHandle, GL_ACTIVE_UNIFORMS, &uniforms);

    GLsizei written; GLint size; GLenum type;
    char* name = new char[length + 1];

#ifdef DEBUG_UNIFORMS
    std::cout << "Uniforms for shader " << m_nVertName << ", " << m_nFragName << "\n";
#endif
    for (int i = 0; i < uniforms; ++i) {
        memset(name, 0, length + 1);
        glGetActiveUniform(m_nShaderHandle, i, length, &written, &size, &type, name);
        m_mapUniformNameIndexMap[name] = glGetUniformLocation(m_nShaderHandle, name);
#ifdef DEBUG_UNIFORMS
        std::cout << "Uniform: " << name << " at " << m_mapUniformNameIndexMap[name] << "\n";
#endif
    }
#ifdef DEBUG_UNIFORMS
    std::cout << "\n";
#endif
    delete[] name;
    glUseProgram(0);

    m_nUniformCount = (int)m_mapUniformNameIndexMap.size();
}

void Shader::PopulateAttributes() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    glUseProgram(m_nShaderHandle);
    GLint   attributes, length;
    glGetProgramiv(m_nShaderHandle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &length);
    glGetProgramiv(m_nShaderHandle, GL_ACTIVE_ATTRIBUTES, &attributes);

    GLsizei written; GLint size; GLenum type;
    char* name = new char[length + 1];

#ifdef DEBUG_ATTRIBUTES
    std::cout << "Attributes for shader " << m_nVertName << ", " << m_nFragName << "\n";
#endif
    for (int i = 0; i < attributes; ++i) {
        memset(name, 0, length + 1);
        glGetActiveAttrib(m_nShaderHandle, i, length, &written, &size, &type, name);
        m_mapAttributeNameIndexMap[name] = glGetAttribLocation(m_nShaderHandle, name);
#ifdef DEBUG_ATTRIBUTES
        std::cout << "Attribute: " << name << " at " << m_mapAttributeNameIndexMap[name] << "\n";
#endif
    }
#ifdef DEBUG_ATTRIBUTES
    std::cout << "\n";
#endif
    delete[] name;
    glUseProgram(0);

    m_nAttributeCount = (int)m_mapAttributeNameIndexMap.size();
}

void Shader::Bind() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    glUseProgram(m_nShaderHandle);
}

void Shader::Unbind() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    m_nLastBoundTextureIndex -= 1;
    while (m_nLastBoundTextureIndex >= 0) {
        glActiveTexture(GL_TEXTURE0 + m_nLastBoundTextureIndex);
        glBindTexture(GL_TEXTURE_2D, 0);
        m_nLastBoundTextureIndex -= 1;
    }
    m_nLastBoundTextureIndex = 0;

    glUseProgram(0);
}

unsigned int Shader::GetHandle() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_nShaderHandle;
}

const char* Shader::GetLastError() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_strLastError;
}

int Shader::GetUniformCount() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_nUniformCount;
}

unsigned int Shader::GetUniformLocation(const char* name) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_mapUniformNameIndexMap[name];
}

std::vector<std::string> Shader::GetUniformNames() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    std::vector<std::string> names;
    names.reserve(m_nUniformCount);

    for(std::map<std::string, unsigned int>::iterator it = m_mapUniformNameIndexMap.begin(); it != m_mapUniformNameIndexMap.end(); it++) {
        names.push_back(it->first);
    }

    return names;
}

int Shader::GetAttribCount() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_nAttributeCount;
}

unsigned int Shader::GetAttribLocation(const char* name) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    return m_mapAttributeNameIndexMap[name];
}

bool Shader::ContainsAttribute(const char* name) {
    return glGetAttribLocation(m_nShaderHandle, name) >= 0;
}

bool Shader::ContainsUniform(const char* name) {
    return glGetUniformLocation(m_nShaderHandle, name) >= 0;
}

std::vector<std::string> Shader::GetAttribNames() {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);

    std::vector<std::string> names;
    names.reserve(m_nAttributeCount);

    for(std::map<std::string, unsigned int>::iterator it = m_mapAttributeNameIndexMap.begin(); it != m_mapAttributeNameIndexMap.end(); it++) {
        names.push_back(it->first);
    }

    return names;
}

void Shader::Bool(const char* name, bool value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform1i(index, value);
#ifdef PRINT_SET_VARS
    std::cout << name << ": " << value << "\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Int(const char* name, int value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform1i(index, value);
#ifdef PRINT_SET_VARS
    std::cout << name << ": " << value << "\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Float(const char* name, float value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform1f(index, value);
#ifdef PRINT_SET_VARS
    std::cout << name << ": " << value << "\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Bvec2(const char* name, const glm::bvec2& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform2i(index, value.x, value.y);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Bvec3(const char* name, const glm::bvec3& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform3i(index, value.x, value.y, value.z);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Bvec4(const char* name, const glm::bvec4& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform4i(index, value.x, value.y, value.z, value.w);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ", " << value.w << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Ivec2(const char* name, const glm::ivec2& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform2i(index, value.x, value.y);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Ivec3(const char* name, const glm::ivec3& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform3i(index, value.x, value.y, value.z);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Ivec4(const char* name, const glm::ivec4& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform4i(index, value.x, value.y, value.z, value.w);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ", " << value.w << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Vec2(const char* name, const glm::vec2& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform2f(index, value.x, value.y);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Vec3(const char* name, const glm::vec3& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform3f(index, value.x, value.y, value.z);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Vec4(const char* name, const glm::vec4& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniform4f(index, value.x, value.y, value.z, value.w);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (" << value.x << ", " << value.y << ", " << value.z << ", " << value.w << ")\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Mat2(const char* name, const glm::mat2& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniformMatrix2fv(index, 1, GL_FALSE, glm::value_ptr(value));
#ifdef PRINT_SET_VARS
    std::cout << name << ": (mat2)\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Mat3(const char* name, const glm::mat3& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniformMatrix3fv(index, 1, GL_FALSE, glm::value_ptr(value));
#ifdef PRINT_SET_VARS
    std::cout << name << ": (mat3)\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Mat4(const char* name, const glm::mat4& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glUniformMatrix4fv(index, 1, GL_FALSE, glm::value_ptr(value));
#ifdef PRINT_SET_VARS
    std::cout << name << ": (mat4)\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}

void Shader::Texture(const char* name, class Texture& value) {
    assert(m_strLastError == 0);
    assert(m_nShaderHandle != 0);
    unsigned int index = m_mapUniformNameIndexMap[name];
    glActiveTexture(GL_TEXTURE0 + m_nLastBoundTextureIndex);
    glBindTexture(GL_TEXTURE_2D, value.GetHandle());
    glUniform1i(index, m_nLastBoundTextureIndex);

    m_nLastBoundTextureIndex += 1;
    assert(m_nLastBoundTextureIndex < 5);
#ifdef PRINT_SET_VARS
    std::cout << name << ": (texture2D)\n";
    PrintGLError();
#endif // PRINT_SET_VARS
}
