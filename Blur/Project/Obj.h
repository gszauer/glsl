#ifndef _H_OBJ_
#define _H_OBJ_

#include "OGLHeaders.h"
#include <vector>

class Obj {
public:
    struct Face {
        unsigned int a;
        unsigned int b;
        unsigned int c;
    };
protected:
    std::vector<Face> Faces;
    std::vector<glm::vec3> Positions;
    std::vector<glm::vec3> Normals;
    std::vector<glm::vec2> TexCoords;

    unsigned int VBO;
    unsigned int IBO;
protected:
    Obj();
    Obj(const Obj&);
    Obj& operator=(const Obj&);
public:
    Obj(const char* filename);
    ~Obj();

    int GetIndexCount();
    int GetVertCount();

    const unsigned int* GetFaces();
    const float* GetPositions();
    const float* GetNormals();
    const float* GetTexCoords();

    int GetNumFaces();
    int GetNumPositions();
    int GetNumNormals();
    int GetNumTexcoords();

    void Render(int position);
    void Render(int position, int normal);
    void Render(int position, int normal, int uv);
    void RenderFixedFunction();
};

#endif
