#version 120

varying vec2 texCoord;

uniform sampler2D texture;
uniform vec2 invViewport;

void main() {
	vec4 sum = vec4(0.0);

#ifndef UNROLL_LOOP
	for (int x = -4; x <= 4; ++x) {
		for (int y = -4; y <= 4; ++y) {
			sum += texture2D(texture, vec2(texCoord.x + float(x) * invViewport.x, texCoord.y + float(y) * invViewport.y)) / 81.0;
		}
	}
#else
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-4.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-3.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-2.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (-1.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (0.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (1.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (2.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (3.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (-4.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (-3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (-2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (-1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (0.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (1.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (2.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (3.0) * invViewport.y))  * 0.0123457;
	sum += texture2D(texture, vec2(texCoord.x + (4.0) * invViewport.x, texCoord.y + (4.0) * invViewport.y))  * 0.0123457;
#endif

	gl_FragColor = sum;
}